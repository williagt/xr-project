# ChromaEnigma
ChromaEnigma is a Unity project where the player uses platforming and colors to proceed to the next stage.
The project was given in IDATT2505, Extended Reality.

## Technologies used
- Unity Hub 3.3.0
- Unity 2021.3.10f1
- Microsoft Visual Studio 2022
- Blender 3.2

## Installation
1. Clone project. 
2. Download [Unity Hub](https://unity3d.com/get-unity/download).
3. In Unity Hub, navigate to 'Projects'.
4. Click 'Add from disc' and select the correct path to ChromaEnigma.
5. From there, choose which Unity version you want to use for the project, note development happened on version 2021.3.10f1 LTS.

Might not be necessary:
1. In Unity Hub, navigate to 'Installs'. 
2. Click 'Install Editor' and from the Official releases, choose the 2021.3.13f1 LTS version.

If there are any problems along the way, looking at Unity's [documentation](https://docs.unity3d.com/hub/manual/Projects.html) could be of help.

## Run
When ChromaEnigma is opened in the Unity editor, there are multiple ways to run it. 
- You can run it directly in the editor (for the best experience, be sure to open Assets>Scenes>MainMenu before pressing play). Note: some problems may occur with the lighting. [This](https://www.youtube.com/watch?v=8-oXE2NWSLM) video helped me fix that issue.
- You can [build](https://docs.unity3d.com/Manual/PublishingBuilds.html) the project.