using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 1.0f;
    private OutOfBounds bounds;

    private void Start()
    {
        bounds = GameObject.FindGameObjectWithTag("Bounds").GetComponent<OutOfBounds>(); //Getting OutOfBounds instance already present in scene to reset player on hit
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cannon")) return; //Bullet will collide with cannon, ignore that
        if(other.CompareTag("Player") && bounds != null)
        {
            bounds.ResetPlayer(); //Reset player if hit by bullet
        }
        Destroy(gameObject); //Destroy this game object when triggered
    }
}
