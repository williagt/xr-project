using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Start button on main menu
public class StartButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(startGame); //Adding startGame to button's onclick
    }

    //Loads level 1
    private void startGame()
    {
        SceneManager.LoadSceneAsync("Level1");
    }
}
