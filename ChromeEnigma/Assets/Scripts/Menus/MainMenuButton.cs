using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(loadMainMenu); //Adding quitGame to button's onclick
    }

    //Quits game
    private void loadMainMenu()
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }
}
