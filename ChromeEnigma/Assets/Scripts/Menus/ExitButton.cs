using UnityEngine;
using UnityEngine.UI;

public class ExitButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(quitGame); //Adding quitGame to button's onclick
    }

    //Quits game
    private void quitGame()
    {
        Application.Quit();
    }
}
