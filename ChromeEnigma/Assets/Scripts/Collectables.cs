using Newtonsoft.Json.Bson;
using UnityEngine;

//Keeps track of collectables. Currently only contains info about whether key is collected or not.
public class Collectables : MonoBehaviour
{
    private bool keyAcquired = false; //Does not initially have key

    //Called when key is collected
    public void gotKey()
    {
        keyAcquired = true;
    }

    public void lostKey()
    {
        keyAcquired = false;
    }

    //Checks if key collected or not
    public bool hasKey()
    {
        return keyAcquired;
    }
}
