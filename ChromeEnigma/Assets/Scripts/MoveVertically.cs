using UnityEngine;

public class MoveVertically : MonoBehaviour
{
    public float topTravel = 10.0f;
    public float bottomTravel = 0.0f;
    public float travelTime = 10.0f;

    private Vector3 topPosition;
    private Vector3 bottomPosition;

    private CharacterController playerController;
    private Rigidbody rb;

    private Vector3 currentPos;

    // Start is called before the first frame update
    void Start()
    {
        //Calculating top and bottom posisiton
        topPosition = transform.position + new Vector3(0, topTravel, 0); 
        bottomPosition = transform.position + new Vector3(0, bottomTravel, 0);
        //Getting rigid body
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Calculating path which rigid body is to travel to, and moving it
        currentPos = Vector3.Lerp(bottomPosition, topPosition,
            Mathf.Cos(Time.time / travelTime * Mathf.PI * 2) * -.5f + .5f);
        rb.MovePosition(currentPos);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Getting CharacterController of player
        if (other.tag == "Player")
            playerController = other.GetComponent<PlayerController>().GetComponent<CharacterController>();
    }

    private void OnTriggerStay(Collider other)
    {
        //Move player with the same speed as elevator as to avoid jitter
        if (other.tag == "Player") {
            playerController.Move(rb.velocity * Time.deltaTime);
        }
    }

}
