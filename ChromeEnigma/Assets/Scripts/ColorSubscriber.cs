using UnityEngine;

//A ColorSubscriber changes properties based on ColorChanger, can be either normal ground or an elevator
public class ColorSubscriber : MonoBehaviour
{
    private Material material; //Contains color of subscriber
    private Collider mCollider; //Turned on or off based on color of ColorChanger
    private Rigidbody rb; //Elevators have rigid bodies, ground does not

    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<Renderer>().material;
        mCollider = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
    }

    //Abstracting away which notification that has to be made
    public void notifyColorChange(Color c) 
    {
        if(CompareTag("GroundColored"))
        {
            notifyColorChangeGround(c);
        }
        if(CompareTag("ElevatorColored"))
        {
            notifyColorChangeElevator(c);
        }
    }

    //If GameObject has tag GroundColored, this will be called
    private void notifyColorChangeGround(Color c)
    {
        //If all rgb values match, make subscriber interactable
        if (c.r == material.color.r && c.g == material.color.g && c.b == material.color.b)
        {
            mCollider.enabled = true;
            material.color = new Color(c.r, c.g, c.b);
        }
        //Else, gray it out and remove interactibility
        else
        {
            mCollider.enabled = false;
            material.color = new Color(material.color.r, material.color.g, material.color.b, 0.1f);
        }
    }

    //If GameObject has tag ElevatorColored, this will be called
    private void notifyColorChangeElevator(Color c)
    {
        //If all rgb values match, make subscriber interactable
        if (c.r == material.color.r && c.g == material.color.g && c.b == material.color.b)
        {
            mCollider.enabled = true;
            rb.detectCollisions = true;
            material.color = new Color(c.r, c.g, c.b);
        }
        //Else, gray it out and remove interactibility
        else
        {
            mCollider.enabled = false;
            rb.detectCollisions = false;
            material.color = new Color(material.color.r, material.color.g, material.color.b, 0.1f);
        }
    }
}
