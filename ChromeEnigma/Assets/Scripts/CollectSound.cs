using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectSound : MonoBehaviour
{
    public AudioSource collectSound;

    public void Play()
    {
        collectSound.Play();
    }
}
