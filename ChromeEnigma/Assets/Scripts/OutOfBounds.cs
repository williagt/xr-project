using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : MonoBehaviour
{
    public GameObject player;
    public GameObject controller;

    private Key key;
    private Collectables collectables;
    private PlayerController playerController;
    // Start is called before the first frame update
    void Start()
    {
        key = controller.GetComponent<Key>();
        collectables = player.GetComponent<Collectables>();
        playerController = player.GetComponent<PlayerController>();
    }

    //If player goes out of bounds, reset key info and player
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        ResetPlayer();
    }

    public void ResetPlayer()
    {
        key.resetKey();
        collectables.lostKey();
        playerController.resetPosition();
    }
}
