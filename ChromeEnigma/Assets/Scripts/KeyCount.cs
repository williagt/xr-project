using UnityEngine;
using TMPro;

public class KeyCount : MonoBehaviour
{
    public TMP_Text controllerCount; //UI string
    public int maxControllers; //Max controllers, or keys, in the level

    // Start is called before the first frame update
    void Start()
    {
        controllerCount.text = "0 / " + maxControllers; //Initializing UI for keeping track of collected key(s)
    }

    //Will update UI string with newCount amount of keys collected
    public void UpdateControllerCount(int newCount)
    {
        controllerCount.text = newCount + " / " + maxControllers;
    }
}
