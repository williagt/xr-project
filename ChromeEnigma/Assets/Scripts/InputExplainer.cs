using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputExplainer : MonoBehaviour
{
    public GameObject inputExplainerUI;
    public static bool inputExplained = true; //Used to allow/disallow use of esc key in PauseMenu
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("ShowInputInfo", 1);
    }

    //Shows input info after given time
    IEnumerator ShowInputInfo(float time)
    {
        yield return new WaitForSeconds(time); //coroutine waits here

        AudioListener.pause = true;
        inputExplained = false;
        inputExplainerUI.SetActive(true);
        Time.timeScale = 0;
        PauseMenu.isPaused = true;
    }

    //OnClick for OK button
    public void OKAndUnpause()
    {
        AudioListener.pause = false;
        inputExplainerUI.SetActive(false);
        Time.timeScale = 1;
        PauseMenu.isPaused = false;
        inputExplained = true;
    }
}
