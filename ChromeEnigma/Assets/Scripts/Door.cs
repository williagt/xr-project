using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    public string nextLevelName;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) //If the one that collided with the door is the player
        {
            bool hasKey = other.GetComponent<Collectables>().hasKey();
            if(hasKey) //If the player has the key
            {
                if(!string.IsNullOrEmpty(nextLevelName))
                {
                    SceneManager.LoadSceneAsync(nextLevelName); //Load next level if defined
                }
            }
        }
    }
}
