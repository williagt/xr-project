using Unity.VisualScripting;
using UnityEngine;

public class Key : MonoBehaviour
{
    public GameObject controllerKeyCounterTextContainer;
    public GameObject ingameKey;
    public GameObject player;
    public GameObject container;
    private KeyCount keyCount;
    private int currentKeyCount; //Consider making it static if more keys recquired per level, but remember to reset when level is done

    // Start is called before the first frame update
    void Start()
    {
        keyCount = controllerKeyCounterTextContainer.GetComponent<KeyCount>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 50.0f * Time.deltaTime, 0, Space.World); //Rotate controller, or key, around itself on an angle
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) //If player collides with key
        {
            container.GetComponent<CollectSound>().Play(); //Plays sound when collected
            currentKeyCount++; //Update key count
            keyCount.UpdateControllerCount(currentKeyCount); //Notify UI counter
            player = other.gameObject;
            player.GetComponent<Collectables>().gotKey(); //Set player state
            ingameKey.SetActive(false); //Deactivate key
        }
    }

    public void resetKey()
    {
        if(player == null || currentKeyCount == 0) return; //Player has not yet acquired the key if it is null and cant reset if count is 0

        currentKeyCount = 0; //Decresaing keycount
        keyCount.UpdateControllerCount(currentKeyCount); //Notify UI counter
        player.GetComponent<Collectables>().lostKey(); //Set player state
        ingameKey.SetActive(true); //Reactivating key
    }
}
