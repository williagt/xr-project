using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float G = -9.81f;

    private CharacterController controller;
    private Animator animator;
    private Vector3 playerVelocity;
    private Vector3 playerStartPosition;
    private Vector3 move = Vector3.zero;

    public float playerSpeed = 5f;
    public float distanceToGround = 0.1f;
    public AudioSource footSteps;
    private bool grounded;

    // Start is called before the first frame update
    void Start()
    {
        controller = gameObject.AddComponent<CharacterController>();
        animator = gameObject.GetComponent<Animator>(); 

        //Setting values for CharacterController
        controller.height = 6.0f;
        controller.center = new Vector3(0, 3, 0);
        controller.radius = 1.5f;

        //Saving start position 
        playerStartPosition = transform.position;
    }

    //Unused, maybe usable if raycast is not behaving
    RaycastHit hit;
    private int raycastLayerMask = ~(1 << 3); //Making raycast ignore layer 3 (bounds)

    // Update is called once per frame
    void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, out hit, distanceToGround, raycastLayerMask); //Checking whether player is touching the ground or not
        if (grounded && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        //Moving player in direction given by player, being minimally affected by size of vector
        move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        move = Vector3.ClampMagnitude(move, 1f);
        controller.Move(move * Time.deltaTime * playerSpeed);
        
        if (move != Vector3.zero) //If moving
        {
            gameObject.transform.forward = move; //Changing forward direction to direction of player moves in
            walkAnimation(); //Activate waling animation
        }
        else
        {
            idleAnimation(); //Else activate idle animation if not moving
        }

        //Applying gravity
        playerVelocity.y += G * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (Time.timeScale == 0) footSteps.Pause();
    }

    //Starts walk animation and starts footstep sounds if on the ground
    private void walkAnimation()
    {
        animator.SetTrigger("Walk");
        animator.ResetTrigger("Stop");
        if (footSteps != null)
        {
            if(grounded) footSteps.enabled = true;
            if(!grounded) footSteps.enabled = false;
        }
    }

    //Starts idle animation and turns off footstep sounds
    private void idleAnimation()
    {
        animator.SetTrigger("Stop");
        animator.ResetTrigger("Walk");
        if (footSteps != null)
        {
            footSteps.enabled = false;
        }
    }

    //Resets player to start position
    public void resetPosition()
    {
        transform.position = playerStartPosition;
    }
}
