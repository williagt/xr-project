using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public GameObject bullet;
    public float timeBetweenBullets = 4.0f;
   
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Shoot", timeBetweenBullets, timeBetweenBullets);
    }

    private void Shoot()
    {
        GameObject bulletCopy = Instantiate(bullet, transform.position, transform.rotation); //Instantiates bullets the same way this game object is facing
        bulletCopy.GetComponent<Renderer>().material = GetComponent<Renderer>().material; //Setting material of bullet to the same as cannon
    }
}
