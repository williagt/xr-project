using System.Linq;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    private SkinnedMeshRenderer userrenderer;
    private Material screenMaterial;

    private GameObject[] groundColored;
    private GameObject[] elevatorColored;
    
    private GameObject[] subscribers;

    // Start is called before the first frame update
    void Start()
    {
        //Getting renderer of player to get it's materials
        userrenderer = GetComponentInChildren<SkinnedMeshRenderer>(); //Recursive dfs, might be a bad idea if gameobject has many childern
        Material[] materials = userrenderer.materials;

        //Extracting the screen's material
        foreach(Material m in materials)
        {
            if (m.name.Contains("Screen"))
            {
                screenMaterial = m;
            }
        }

        //Collecting all color subscribers
        if (groundColored == null) groundColored = GameObject.FindGameObjectsWithTag("GroundColored");
        if (elevatorColored == null) elevatorColored = GameObject.FindGameObjectsWithTag("ElevatorColored");
        subscribers = groundColored.Concat(elevatorColored).ToArray(); //Putting all subscribers here

        //Updating color subscribers
        foreach(GameObject g in subscribers)
        {
            if (g != null) g.GetComponent<ColorSubscriber>().notifyColorChange(screenMaterial.color);
        }
    }

    // Update is called once per frame, used for changing colors based on if user presses 1, 2, 3 or r
    void Update()
    {
        //Interact with red if pressed 1
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            screenMaterial.color = Color.red;
            foreach (GameObject g in subscribers)
            {
                if (g != null) g.GetComponent<ColorSubscriber>().notifyColorChange(Color.red);           
            }
        }

        //Interact with green if pressed 2
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            screenMaterial.color = Color.green;
            foreach (GameObject g in subscribers)
            {
                if (g != null) g.GetComponent<ColorSubscriber>().notifyColorChange(Color.green);
            }
        }
        
        //Interact with blue if pressed 3
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            screenMaterial.color = Color.blue;
            foreach (GameObject g in subscribers)
            {
                if (g!=null) g.GetComponent<ColorSubscriber>().notifyColorChange(Color.blue);
            }
        }

        //Change to yellow if red + green
        if(Input.GetKey(KeyCode.Alpha1) && Input.GetKey(KeyCode.Alpha2))
        {
            Color yellow = new Color(1,1,0,1); //Had to define yellow as this, predefined did not play well with rest of setup
            screenMaterial.color =yellow;
            foreach (GameObject g in subscribers)
            {
                if (g != null) g.GetComponent<ColorSubscriber>().notifyColorChange(yellow);
            }
        }

        //Change to purple if red + blue
        if (Input.GetKey(KeyCode.Alpha1) && Input.GetKey(KeyCode.Alpha3))
        {
            screenMaterial.color = Color.magenta;
            foreach (GameObject g in subscribers)
            {
                if (g != null) g.GetComponent<ColorSubscriber>().notifyColorChange(Color.magenta);
            }
        }

        //Change to cyan if green + blue
        if (Input.GetKey(KeyCode.Alpha2) && Input.GetKey(KeyCode.Alpha3))
        {
            screenMaterial.color = Color.cyan;
            foreach (GameObject g in subscribers)
            {
                if (g != null) g.GetComponent<ColorSubscriber>().notifyColorChange(Color.cyan);
            }
        }

        //Remove all colors if pressed r
        if (Input.GetButtonDown(KeyCode.R.ToString()))
        {
            screenMaterial.color = Color.white;
            foreach (GameObject g in subscribers)
            {
                if (g != null) g.GetComponent<ColorSubscriber>().notifyColorChange(Color.white);
            }
        }
    }
}
